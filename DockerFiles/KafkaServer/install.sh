mkdir /usr/local/java
tar -zxf jre.gz -C /usr/local/java/
tar -zxf kafka.tgz -C /usr/local/
ln -s /usr/local/kafka_2.12-2.1.0 /kafka

## 需要更改server.properties 增加kafka服务器的监听
sed -i 's/#listeners=PLAINTEXT:\/\/:9092/listeners=PLAINTEXT:\/\/kafkaserver:9092/g' /kafka/config/server.properties

/kafka/bin/zookeeper-server-start.sh /kafka/config/zookeeper.properties &
/kafka/bin/kafka-server-start.sh /kafka/config/server.properties
