mkdir /usr/local/java
tar -zxf jre.gz -C /usr/local/java/
tar -zxf kafka.tgz -C /usr/local/
ln -s /usr/local/kafka_2.12-2.1.0 /kafka
# 生产一条消息

echo helloworld | /kafka/bin/kafka-console-producer.sh --broker-list kafkaserver:9092 --topic test
