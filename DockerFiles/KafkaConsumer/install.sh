mkdir /usr/local/java
tar -zxf jre.gz -C /usr/local/java/
tar -zxf kafka.tgz -C /usr/local/
ln -s /usr/local/kafka_2.12-2.1.0 /kafka
# 创建一个Topic
/kafka/bin/kafka-topics.sh --create --zookeeper kafkaserver:2181 --replication-factor 1 --partitions 1 --topic test
/kafka/bin/kafka-topics.sh --list --zookeeper kafkaserver:2181
# 启动监听进程

/kafka/bin/kafka-console-consumer.sh --bootstrap-server kafkaserver:9092 --topic test --from-beginning