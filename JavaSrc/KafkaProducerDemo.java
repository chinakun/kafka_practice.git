import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.Properties;

public class KafkaProducerDemo {
    private static KafkaProducer<String, String> producer;
    private final static String TOPIC = "hello";
    private int keyCounts=0;
    private static String data;
    private static String env;
    public KafkaProducerDemo(){
    	env = System.getenv("id");
        Properties props = new Properties();
        props.put("bootstrap.servers", "kafkaserver:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        //设置分区类,根据key进行数据分区
        producer = new KafkaProducer<String, String>(props);
    }

	public void produce(String data) {
		String key = String.valueOf(keyCounts);
		producer.send(new ProducerRecord<String, String>(TOPIC, key, data));
		System.out.println(data); //打印data的内容
	}
	public void closeProducer() {
		producer.close();
	}
    public static void main(String[] args) throws InterruptedException, IOException {
    	 //Create new kafkaproducer.
    	 KafkaProducerDemo kafka= new KafkaProducerDemo();
    	 //Create a WatchService case.
         WatchService watchService = FileSystems.getDefault().newWatchService();  
         //Open log file
         File logs = new File("/var/log/nginx/access.log");
         final Path path = Paths.get( "/var/log/nginx/");
         //Register the path of directory.
         final WatchKey watchKey = path.register( watchService, StandardWatchEventKinds.ENTRY_MODIFY ,  
                     StandardWatchEventKinds. ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE );  
         boolean fileNotChanged = true;  
         int count = 0;  
         while (fileNotChanged ) {
            final WatchKey wk = watchService .take();  
            System. out.println("Loop count: " + count );
            for (WatchEvent<?> event : wk .pollEvents()) {
                final Path changed = (Path) event.context();
                System. out.println(changed + ", " + event .kind());  
                if (changed .endsWith("access.log")) {  
                     System. out.println("Nginx log file has changed" );  
                     data = env+": "+readLastLine(logs,null);
                     System. out.println(data);  
                     kafka.produce(data);
                }
            }
            // reset the key  
            boolean valid = wk .reset();  
            if (!valid ) {  
                System. out.println("Key has been unregisterede" );  
                kafka.closeProducer();
            }
        }
    }
    /**
     * 
     * @param file
     * @param charset
     * @return String the last line
     * @throws IOException
     */
    public static String readLastLine(File file, String charset) throws IOException
    {
        if (!file.exists() || file.isDirectory() || !file.canRead())
            return null;
        RandomAccessFile raf = null;
        try
        {
            raf = new RandomAccessFile(file, "r");
            long len = raf.length();
            if (len == 0L)
                return "";
            long pos = len - 1;
            while (pos > 0)
            {
                pos--;
                raf.seek(pos);
                if (raf.readByte() == '\n')
                    break;
            }
            if (pos == 0)
                raf.seek(0);
 
            byte[] bytes = new byte[(int) (len - pos)];
            raf.read(bytes);
 
            if (charset == null)
                return new String(bytes);
 
           return new String(bytes, charset);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (raf != null)
            {
                try
                {
                    raf.close();
                } catch (Exception e2)
                {
                }
            }
        }
        return null;
    }
}