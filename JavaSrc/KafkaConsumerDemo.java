import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.Properties;

public class KafkaConsumerDemo {
    private static KafkaConsumer<String, String> consumer;
    private final static String TOPIC = "hello";
    public KafkaConsumerDemo(){
        Properties props = new Properties();
        props.put("bootstrap.servers", "kafkaserver:9092"); //Kafka server的地址，这里没有使用broker
        //每个消费者分配独立的组号
        props.put("group.id", "test2");
        //如果value合法，则自动提交偏移量
        props.put("enable.auto.commit", "true");
        //设置多久一次更新被消费消息的偏移量
        props.put("auto.commit.interval.ms", "1000");
        //设置会话响应的时间，超过这个时间Kafka可以选择放弃消费或者消费下一条消息
        props.put("session.timeout.ms", "30000");
        //自动重置offset
        props.put("auto.offset.reset","earliest");
        props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<String, String>(props); //生成新的 Kafka consumer
    }
	public void consume(OutputStreamWriter writer){
        consumer.subscribe(Arrays.asList(TOPIC));
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records){
                System.out.printf("%s \n",record.value());
                //Append the message into a log file.
                try {
					writer.append(record.value());
					writer.append("\n");
					writer.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        }
    }
    public static void main(String[] args) {
    	// Open o new log file on the kafka consumer in the directory "/data".
    	File file = new File("/data/nginx.log");
    	// Open a output stream for the witer.
    	FileOutputStream stream = null;
		try {
			stream = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	OutputStreamWriter writer= new OutputStreamWriter(stream);
    	//Pass the writer to the kafka consumer core.
        new KafkaConsumerDemo().consume(writer);
        try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
