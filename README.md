# kafka容器分布式部署实战

#### 项目介绍
该项目是为学习kafka与容器部署而准备的，如果你是想通过该项目学习，请仔细阅读各方面代码。
Kafka版本:2.12 Jre|Jdk版本:8u191 docker版本:18.09 IDE:Eclipse k8s版本:v1.12.3

#### 安装教程与使用说明

1. 将附件中jar.gz 与 kafka.tgz 复制到项目DockerFiles目录下的每个文件夹中。
2. 在每个DockerFiles目录下的文件夹中运行Docker build -t yufeng/kafkaxxx:v1(当然你也可以制作自己的标签，但是需要自己更改k8s部署文件。) 
3. 进入k8s-deployment 文件夹下运行create.sh。
4. 检测kafkaconsumer 输出日志，查看有无接收到消息。
5. 运行drop.sh清理容器。

#### 新的项目资源 （Kafka实战项目第二阶段）已经添加完毕，提交了新的镜像构建文件，但是不包含K8S部署文件。
1、包含了Java版本的KafkaConsumer容器镜像。
2、包含了集成了KafkaProducer的Nginx容器镜像。
3、包含了 KafkaConsumer和KafkaProducer 的Java源文件，但不包含Jar包，对应的Jar包位于项目附件中，Kafka版本为2.12。
